#include "stdafx.h"

#include "MonsterAI.h"
#include "UseManager.h"
#include "WeenieFactory.h"
#include "World.h"
#include "WorldLandBlock.h"
#include "PetDevice.h"

PetDevice::PetDevice()
{
}

PetDevice::~PetDevice()
{
}

void PetDevice::LoadEx(class CWeenieSave &save)
{
	CWeenieObject::LoadEx(save);
}

int PetDevice::Use(CPlayerWeenie *player)
{
	if (!player)
		return WERROR_OBJECT_GONE;

	if (player->IsInPortalSpace())
	{
		player->NotifyUseDone(WERROR_ACTIONS_LOCKED);
		return WERROR_NONE;
	}

	if (!player->FindContainedItem(GetID()))
	{
		player->NotifyUseDone(WERROR_NONE);
		return WERROR_NONE;
	}

	DWORD pet_id = player->InqIIDQuality(PET_IID, 0);
	CWeenieObject *pet = nullptr;
	
	// see if it exists and if it is ours
	if (pet_id)
		pet = g_pWorld->FindObject(pet_id);
	if (pet && pet->InqIIDQuality(PET_OWNER_IID, 0) == player->GetID())
	{
		if (InqBoolQuality(UNLIMITED_USE_BOOL, 0) || InqIntQuality(MAX_STRUCTURE_INT, 0) == 0)
		{
			// unlimited use, despawn pet
			DespawnPet(player, pet_id);
		}
		else
		{
			// limited use, error
			player->NotifyUseDone(WERROR_ALREADY_BEING_USED);
			return WERROR_NONE;
		}
	}
	else
	{
		// error if out of uses
		if (InqIntQuality(STRUCTURE_INT, 0, TRUE) <= 0)
		{
			player->NotifyUseDone(WERROR_MAGIC_GENERAL_FAILURE);
			return WERROR_NONE;
		}

		SpawnPet(player);
	}

	CGenericUseEvent *useEvent = new CGenericUseEvent;
	useEvent->_target_id = GetID();
	player->ExecuteUseEvent(useEvent);

	return WERROR_NONE;
}

int PetDevice::DoUseResponse(CWeenieObject *player)
{
	DecrementStackOrStructureNum(1, false);
	return WERROR_NONE;
}

void PetDevice::InventoryTick()
{
	CWeenieObject::InventoryTick();
}

void PetDevice::Tick()
{
	CWeenieObject::Tick();
}

void PetDevice::SpawnPet(CPlayerWeenie *player)
{
	DWORD pet_wcid = (DWORD)InqIntQuality(PET_CLASS_INT, 0, TRUE);
	if (pet_wcid)
	{
		CWeenieObject *weenie = g_pWeenieFactory->CreateWeenieByClassID(pet_wcid, nullptr, false);

		// Move outside collision sphere?
		Position pos = player->GetPosition();
		float heading = pos.frame.m_angles.w; // *0.0174532925;
		pos.frame.m_origin.x += (2.5 * cos(heading));
		pos.frame.m_origin.y += (2.5 * sin(heading));
		//CSphere *sp = player->GetSphere();
		//CSphere *sw = weenie->GetSphere();
		//float radius = sp->radius + sw->radius;
		weenie->SetInitialPosition(pos);

		// copy / setup props
		weenie->m_Qualities.SetInstanceID(PET_OWNER_IID, player->GetID());
		weenie->m_Qualities.SetInstanceID(PET_DEVICE_IID, GetID());

		weenie->SetName(csprintf("%s's %s", player->GetName().c_str(), weenie->GetName().c_str()));

		weenie->CopyIntStat(GEAR_DAMAGE_INT, this);
		weenie->CopyIntStat(GEAR_DAMAGE_RESIST_INT, this);
		weenie->CopyIntStat(GEAR_CRIT_INT, this);
		weenie->CopyIntStat(GEAR_CRIT_RESIST_INT, this);
		weenie->CopyIntStat(GEAR_CRIT_DAMAGE_INT, this);
		weenie->CopyIntStat(GEAR_CRIT_DAMAGE_RESIST_INT, this);
		weenie->CopyIntStat(GEAR_HEALING_BOOST_INT, this);
		weenie->CopyIntStat(GEAR_NETHER_RESIST_INT, this);
		weenie->CopyIntStat(GEAR_LIFE_RESIST_INT, this);
		weenie->CopyIntStat(GEAR_MAX_HEALTH_INT, this);

		double lifespan = 0;
		if (lifespan = InqFloatQuality(COOLDOWN_DURATION_FLOAT, 0, TRUE) && lifespan > 0)
		{
			weenie->_timeToRot = Timer::cur_time + lifespan;
			weenie->m_Qualities.SetInt(LIFESPAN_INT, 1);
		}

		if (g_pWorld->CreateEntity(weenie))
		{
			player->m_Qualities.SetInstanceID(PET_IID, weenie->GetID());
		}
	}
}

void PetDevice::DespawnPet(CPlayerWeenie *player, DWORD petId)
{
	CWeenieObject *pet = g_pWorld->FindObject(petId, false, false);

	if (pet)
	{
		// which?
		pet->Remove();
		//MarkForDestroy();
	}
	if (player)
		player->m_Qualities.SetInstanceID(PET_IID, 0);
}

///////////////////////////////////////////////////////////////////////////////
// 
//
PetPassive::PetPassive()
{

}

PetPassive::~PetPassive()
{

}

void PetPassive::PostSpawn()
{
	CMonsterWeenie::PostSpawn();
	if (!m_MonsterAI)
		m_MonsterAI = new MonsterAIManager(this, m_Position);
}

void PetPassive::Tick()
{
	CMonsterWeenie::Tick();

	DWORD oid = InqIIDQuality(PET_OWNER_IID, 0);
	CWeenieObject *owner = nullptr;

	if (oid)
		owner = g_pWorld->FindPlayer(oid);

	if (!owner)
	{
		// no master
		Remove();
		return;
	}

	CSphere *sm = this->GetSphere();
	CSphere *so = owner->GetSphere();
	float radius = sm->radius + so->radius;
	// TODO: Better way of handling follow
	float distance = DistanceTo(owner);
	if (distance - radius > 1.0f)
	{
		m_MonsterAI->SetHomePosition(owner->m_Position);
		m_MonsterAI->SwitchState(ReturningToSpawn);
	}

}

void PetPassive::OnDeath(DWORD killer_id)
{
	CWeenieObject::OnDeath(killer_id);

	MakeMovementManager(TRUE);
	StopCompletely(0);

	DWORD oid = InqIIDQuality(PET_OWNER_IID, 0);
	CWeenieObject *owner = nullptr;

	if (oid)
		owner = g_pWorld->FindPlayer(oid);

	if (owner)
		owner->m_Qualities.SetInstanceID(PET_IID, 0);

	MarkForDestroy();
}

///////////////////////////////////////////////////////////////////////////////
//
//
PetCombat::PetCombat()
{
}

PetCombat::~PetCombat()
{
}

bool PetCombat::CanTarget(CWeenieObject* target)
{
	if (target == nullptr)
		return false;

	if (target->AsPlayer())
		return false;

	return target->m_Qualities.m_WeenieType == WeenieType::Creature_WeenieType;
}

void PetCombat::Tick()
{
	CMonsterWeenie::Tick();

	DWORD oid = InqIIDQuality(PET_OWNER_IID, 0);
	DWORD did = InqIIDQuality(PET_DEVICE_IID, 0);

	CWeenieObject *owner = nullptr;
	CWeenieObject *device = nullptr;

	if (oid)
		owner = g_pWorld->FindPlayer(oid);

	if (did)
		device = g_pWorld->FindObject(did, false, false);

	if (!owner)
	{
		// no master
		Remove();
		return;
	}

	// Check lifespan

	// Ensure attacking

	CWeenieObject *target = m_MonsterAI->GetTargetWeenie();
	if (!target || target->IsDead())
	{
		// clear the target
		target = nullptr;

		// ?? COMBAT_PET_RANGE_INT
		float range = (float)InqFloatQuality(VISUAL_AWARENESS_RANGE_FLOAT, 1.0, TRUE);
		std::list<CWeenieObject*> targets;
		g_pWorld->EnumNearby(this, range, &targets);

		float closest = FLT_MAX;
		for (auto weenie : targets)
		{
			float distance = fabs(DistanceTo(weenie));
			if (CanTarget(weenie) && distance < closest)
			{
				closest = distance;
				target = weenie;
			}
		}

		// if new target found
		if (target)
			m_MonsterAI->SetNewTarget(target);
	}
}
